**VI information**

Code for the thesis: Explaining the unemployment gaps between migrants 
and natives in Europe. Most important files are data_mangling.R, batchmen.R 
and batchwomen.R. Other files are dependencies (e.g. creation of group 
variables) or experiments.
