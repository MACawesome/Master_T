rm(list=ls())
setwd("~/School/Thesis")

## Load relevant datasets
load("~/School/Thesis/Repository/essframe.RData")

## Remove superfluous objects
rm(ess.dat, fess.dat, mess.dat)
gc()

## Load function repository 
source(file="./tfunktions.R")

## For ease of use, list of males and females' datasets
bin.list <- list(mbin.dat, fbin.dat)

## Loading dependencies
library(lme4)
library(MASS)
library(MCMCglmm)
library(parallel)
library(coda)
library(optimx)

## Loading the modelled objects for further analyses
load("./Repository/models_ess.RData")

## Heterogeneity of proportions
groupsizes.agg <- with(mbin.dat,
                       tapply(activ, list(cntry, year), function(x) {
                           length(x)
                       })
                       )
groupsizes.sd <- sd(groupsizes.agg, na.rm=TRUE)

## Commenges Heterogeneity Test
lapply(bin.list, function(x) {
     commenges.test(numactiv, c("cntry", "year"), x)
})
## Chi-square test
lapply(bin.list, function(x) {
    input <- with(x, table(numactiv, cntryear))
    chisq.test(input)
})

## Finding the appropriate time variable specification
## m indicates males, f indicates females
roundformulas <- list(m0.for0 = formula(activ ~ 1 + (1 | cntry/year)),
                      m0.for1 = formula(activ ~ 1 + (1 | cntry/year) +
                                            essround),
                      m0.for2 = formula(activ ~ 1 + (1 | cntry/year) +
                                            poly(essround, degree=2)),
                      m0.for3 = formula(activ ~ 1 + (1 | cntry/year) +
                                            poly(essround, degree=3)),
                      m0.for4 = formula(activ ~ 1 + (1 | cntry/year) +
                                            as.factor(essround)))

m0.output <- mclapply(roundformulas, function(x) {
    glmer(x, family=binomial, data=mbin.dat, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=2e4)))},
    mc.cores=3)

f0.output <- mclapply(roundformulas, function(x) {
    glmer(x, family=binomial, data=fbin.dat, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=2e4)))},
    mc.cores=3)

## Change the m's to f's
names(f0.output) <- sub("m", "f", names(f0.output))

for(i in seq_along(m0.output)) {
    current.assign <- paste0("m0.fit", i-1)
    assign(current.assign, m0.output[[i]])
    current.assign <- paste0("m0.sum", i-1)
    assign(current.assign, summary(m0.output[[i]]))
}

for(i in seq_along(f0.output)) {
    current.assign <- paste0("f0.fit", i-1)
    assign(current.assign, f0.output[[i]])
    current.assign <- paste0("f0.sum", i-1)
    assign(current.assign, summary(f0.output[[i]]))
}

## Based on the two indices and pragmatic considerations
## I choose to go with a linear specification of time
getlistIC(f0.output)
getlistIC(m0.output)

## Adding individual variables for males and females in
## a distinct manner
mind.cfor1 <- formula(activ ~ 1 + feduc + poly(sagea, 2, raw=TRUE) +
                         (1 | cntry/year) + essround)
mind.cfor2 <- formula(activ ~ 1 + feduc + sagea + (1 | cntry/year) + essround)
## cfor3 is the most appropriate (lowest BIC)
mind.cfor3 <- formula(activ ~ 1 + feduc + poly(sagea, 3, raw=TRUE) +
                          (1 | cntry/year) + essround)

mind.list <- list(mind.cfor1 = mind.cfor1,
                  mind.cfor2 = mind.cfor2,
                  mind.cfor3 = mind.cfor3)


mind.output <- mclapply(mind.list, function(x) {
    glmer(x, data=mbin.dat, family=binomial, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=2e4)))
}, mc.cores=3)

## Both indices indicate that the more complex third-order
## specification for age is the best one
getlistIC(mind.output)

## Specification of individual control variables for the female population
find.cfor1 <- formula(activ ~ 1 + feduc + poly(sagea, 3, raw=TRUE) +
                          (1 | cntry/year) + essround)
find.cfor2 <- formula(activ ~ 1 + feduc + poly(sagea, 2, raw=TRUE) +
                         (1 | cntry/year) + essround)
find.cfor3 <- formula(activ ~ 1 + feduc + sagea + (1 | cntry/year) + essround)
find.cfor4 <- formula(activ ~ 1 + educ +
                          sagea + (1 | cntry/year) + essround)
find.cfor5 <- formula(activ ~ 1 + educ + 
                          poly(sagea, 2, raw=TRUE) + (1 | cntry/year) + essround)
## Trying out a specification with the time as a fixed effect
## This is the model I choose to build on (lowest BIC)
find.cfor6 <- formula(activ ~ 1 + educ +
                          poly(sagea, 3, raw=TRUE) + (1 | cntry/year) +
                          essround)

find.list <- list(find.cfor1 = find.cfor1,
                  find.cfor2 = find.cfor2,
                  find.cfor3 = find.cfor3,
                  find.cfor4 = find.cfor4,
                  find.cfor5 = find.cfor5,
                  find.cfor6 = find.cfor6)

find.output <- mclapply(find.list, function(x) {
    glmer(x, data=fbin.dat, family=binomial, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=2e4)))
}, mc.cores=3)

## Although the AIC quite clearly indicate that the more
## complicated specification of education gets a better fit,
## I prefer the three categories one based on the BIC and
## pragmatic reasons
getlistIC(find.output)

## Progressively adding the critical individual variables
## For males
mcritind.for1 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + (1|cntry/year))
mcritind.for2 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + speakoff +
                             (1|cntry/year))
mcritind.for3 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + islam +
                             (1|cntry/year))
## The AIC (because the number of parameter is growing to be pretty large)
## is smallest for the model below with all three "critical"
## covariates
## The BIC however indicates that this models is not as parsimonious as
## the best fitting model with only the control variables
mcritind.for4 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + speakoff + islam +
                             (1|cntry/year))

mcritind.list <- list(mcritind.for1 = mcritind.for1,
                      mcritind.for2 = mcritind.for2,
                      mcritind.for3 = mcritind.for3,
                      mcritind.for4 = mcritind.for4)

mcritind.output <- mclapply(mcritind.list, function(x) {
    glmer(x, data=mbin.dat, family=binomial, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=2e4)))
}, mc.cores=3)

## The model which includes all three critical variables
## is kept for the reasons cited higher
getlistIC(mcritind.output)


## For females
fcritind.for1 <- formula(activ ~ feduc +
                             poly(sagea, 3, raw=TRUE) +
                             essround + gener + (1|cntry/year))
fcritind.for2 <- formula(activ ~ feduc +
                             poly(sagea, 3, raw=TRUE) +
                             essround + gener + speakoff +
                             (1|cntry/year))
fcritind.for3 <- formula(activ ~ feduc +
                             poly(sagea, 3, raw=TRUE) +
                             essround + gener + islam +
                             (1|cntry/year))
## This model which includes Islam has a slightly lower
## AIC than the preceding one. Additionally, it has the lowest
## AIC score out of the models tested for the female pop.
fcritind.for4 <- formula(activ ~ feduc +
                             poly(sagea, 3, raw=TRUE) +
                             essround + gener + speakoff + islam +
                             (1|cntry/year))

fcritind.list <- list(fcritind.for1 = fcritind.for1,
                      fcritind.for2 = fcritind.for2,
                      fcritind.for3 = fcritind.for3,
                      fcritind.for4 = fcritind.for4)

fcritind.output <- mclapply(fcritind.list, function(x) {
    glmer(x, data=fbin.dat, family=binomial, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=2e4)))
}, mc.cores=3)

## Same conclusion for the females as for the males before
## the more complex models here is chosen
getlistIC(fcritind.output)

## Adding random slopes to the critical variables in the models
## testing single and double random slopes 
## For the males
## Variation only per country
mcntrslope.for1 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + islam +
                             (1 + gener|cntry:year) + (1 | cntry))
mcntrslope.for2 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + islam +
                             (1 + islam|cntry:year) + (1|cntry))
mcntrslope.for3 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                              essround + gener + speakoff + islam +
                              (1 + gener | cntry:year) + (1 + gener |cntry))

mcntrslope.list <- list(mcntrslope.for1 = mcntrslope.for1,
                       mcntrslope.for2 = mcntrslope.for2,
                       mcntrslope.for3 = mcntrslope.for3)

mcntrslope.output <- mclapply(mcntrslope.list, function(x) {
    glmer(x, data=mbin.dat, family=binomial, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=1e4)))
}, mc.cores=3)

getlistIC(mcntrslope.output)

## LikRatio test for the  slopes
anova(mcntrslope.output[[2]], mcntrslope.output[[3]])

## Random slope for the females
fcntrslope.for1 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + islam +
                             (1 + gener|cntry:year) + (1|cntry))
fcntrslope.for2 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                             essround + gener + islam +
                             (1 + gener|cntry:year) + (1 + gener |cntry))
## This one doesn't converge readily
fcntrslope.for3 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                              essround + gener + islam +
                              (1 + gener | cntry:year) +
                              (1 | cntry))

fcntrslope.list <- list(fcntrslope.for1 = fcntrslope.for1,
                       fcntrslope.for2 = fcntrslope.for2,
                       fcntrslope.for3 = fcntrslope.for3)

fcntrslope.output <- mclapply(fcntrslope.list, function(x) {
    glmer(x, data=fbin.dat, family=binomial, nAGQ=1,
          control=glmerControl(optimizer="bobyqa",
                               optCtrl=list(maxfun=2e4)))
}, mc.cores=3)

getlistIC(fcntrslope.output)

## Combined run for both females and males
cntrslope.list <- unlist(list(mcntrslope.list, fcntrslope.list))

## Can be expected to run for about 30 to 45 minutes at least
cntrslope.output <- mclapply(seq_along(cntrslope.list), function(i) {
    if(i < 4) {
        glmer(cntrslope.list[[i]], data=mbin.dat, family=binomial,
              nAGQ=1, control=glmerControl(optimizer="bobyqa",
                                           optCtrl=list(maxfun=2e4)))
    }
    else if(i >= 4) {
        glmer(cntrslope.list[[i]], data=fbin.dat, family=binomial,
              nAGQ=1, control=glmerControl(optimizer="bobyqa",
                                           optCtrl=list(maxfun=2e4)))
    }
}, mc.cores=3)

relgrad <- with(f_macro.output[[3]]@optinfo$derivs,solve(Hessian,gradient))
max(abs(relgrad))

## Adding the macro-effects and the interactions within country
f_macro.for1 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) + essround +
                            speakgen + islam + (1 + speakgen |cntry:year) +
                            (1| cntry) + m_gdppc + c_gdppc +
                            m_olddeprat + c_olddeprat + m_forde + c_forde)
f_macro.for2 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                                essround + speakgen +
                                islam + (1 |cntry:year) +
                                (1 | cntry) + m_gdppc + c_gdppc + m_olddeprat +
                                c_olddeprat + m_forde + c_forde +
                                c_forde:fgen + c_forde:sgen)
f_macro.for3 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                                essround + fgen + sgen + fgen:speakoff +
                                islam + (1 + fgen + sgen |cntry:year) +
                                (1 | cntry) + m_gdppc + c_gdppc + m_olddeprat +
                                c_olddeprat + m_forde + c_forde +
                                c_forde:fgen + c_forde:sgen +
                                c_olddeprat:fgen + c_olddeprat:sgen)

f_macro.list <- list(f_macro.for1 = f_macro.for1,
                     f_macro.for2 = f_macro.for2,
                     f_macro.for3 = f_macro.for3)

f_macro.output <- mclapply(f_macro.list, function(x) {
    glmer(x, data=fbin.dat, family=binomial,
          nAGQ=1, control=glmerControl(optimizer="bobyqa"))
}, mc.cores=3)

## With slopes and effects within
m_macro.for1 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                                essround + gener + islam  +
                                (1 + gener | cntry:year) +
                                (1 | cntry) + c_gdppc +
                                 c_gdppc:gener)
m_macro.for2 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) + essround +
                            gener + islam  + (1 + gener | cntry:year) +
                            (1 | cntry) + c_forde +
                            c_forde:gener)
m_macro.for3 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) + essround +
                            gener + islam  + (1 + gener | cntry:year) +
                            (1 | cntry) + m_gdppc + c_gdppc + m_olddeprat +
                            c_olddeprat + m_forde + c_forde +
                            c_olddeprat:gener)
## With slopes and effects at the country level
m_cntmacro.for1 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) +
                                essround + gener + islam  +
                                (1 | cntry:year) +
                                (1 gener | cntry) + m_gdppc + c_gdppc +
                                m_olddeprat + c_olddeprat +
                                m_forde + c_forde + m_gdppc:gener)
m_cntmacro.for5 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) + essround +
                            gener + islam  + speakoff + (1 + gener | cntry:year) +
                            (1 | cntry) + m_forde +
                            m_forde:gener)
m_cntmacro.for6 <- formula(activ ~ feduc + poly(sagea, 3, raw=TRUE) + essround +
                            gener + islam  + speakoff + (1 | cntry:year) +
                            (1 + gener| cntry) + m_olddeprat +
                            m_olddeprat:gener)

m_cntmacro.list <- list(m_macro.for1 = m_macro.for1,
                     m_macro.for2 = m_macro.for2,
                     m_macro.for3 = m_macro.for3,
                     m_cntmacro.for1 = m_cntmacro.for1,
                     m_cntmacro.for2 = m_cntmacro.for2,
                     m_cntmacro.for3 = m_cntmacro.for3)

m_macro.output <- mclapply(m_macro.list, function(x) {
    glmer(x, data=mbin.dat, family=binomial,
          nAGQ=1, control = glmerControl(optimizer="bobyqa",
                                         optCtrl=list(maxfun=1e5)))
}, mc.cores=3)

## Models with varying islam


 

mods.tosave <- c("mind.output", "find.output", "m0.output", "f0.output",
                    "mcritind.output", "fcritind.output",
                    "cntyearrslope.output", "finalrslope.output")

gc()
save(list=mods.tosave, file="./Repository/models_ess.RData")






